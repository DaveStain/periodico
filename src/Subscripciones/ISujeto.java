/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Subscripciones;

/**
 *
 * @author Paco
 */
public interface ISujeto {
    public void entregar();
    public void addObserver(IObservador obs);
    public void rmvObserver(IObservador obs);
}
